module Route exposing (Route(..), parseLocation, routerLink, pathFromRoute)

import Html exposing (Attribute, Html, a, text)
import Html.Attributes exposing (href)
import Html.Events exposing (onWithOptions, defaultOptions)
import Json.Decode as Decode
import Msgs
import Navigation exposing (Location)
import UrlParser exposing (..)


type Route
    = HomeRoute
    | TodosRoute
    | TodoRoute Int
    | NotFoundRoute



{--
Set this to the directory that the app will be served from. If the app is going
to be served from the root directory of your server, set it to "".

In this case it is being served from https://m-burg.gitlab.io/elm-todo-pwa, so
the base path will be elm-todo-pwa.
--}


basePath : String
basePath =
    "elm-todo-pwa"


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map HomeRoute top
        , map TodoRoute (s "todos" </> int)
        , map TodosRoute (s "todos")
        ]


pathFromRoute : Route -> String
pathFromRoute route =
    "/"
        ++ basePath
        ++ case route of
            HomeRoute ->
                "/"

            TodoRoute id ->
                "/todos/" ++ toString id

            TodosRoute ->
                "/todos"

            NotFoundRoute ->
                "/404/"


parseLocation : Location -> Route
parseLocation location =
    case (parsePath (s basePath </> matchers) location) of
        Just route ->
            route

        Nothing ->
            NotFoundRoute


onLinkClick : msg -> Attribute msg
onLinkClick message =
    let
        options =
            { stopPropagation = False
            , preventDefault = True
            }
    in
        onWithOptions
            "click"
            { defaultOptions | preventDefault = True }
            (Decode.andThen (maybePreventDefault message) preventDefaultUnlessKeypressed)


preventDefaultUnlessKeypressed : Decode.Decoder Bool
preventDefaultUnlessKeypressed =
    Decode.map2
        nor
        (Decode.field "ctrlKey" Decode.bool)
        (Decode.field "metaKey" Decode.bool)


nor : Bool -> Bool -> Bool
nor x y =
    not (x || y)


maybePreventDefault : msg -> Bool -> Decode.Decoder msg
maybePreventDefault msg preventDefault =
    if preventDefault == True then
        Decode.succeed msg
    else
        Decode.fail "Delegated to browser default"


routerLink : List (Html.Attribute Msgs.Msg) -> String -> Route -> Html Msgs.Msg
routerLink attrs name target =
    let
        path =
            pathFromRoute target
    in
        a
            ([ href path
             , onLinkClick (Msgs.ChangeLocation path)
             ]
                ++ attrs
            )
            [ text name ]
