module Page.Todos exposing (todos, singleTodo, actionModal)

import Html exposing (Html, Attribute, text, div, h1, img, p, button, form, input, label, fieldset, legend, h1, textarea, li, ul, hr, h3, span)
import Html.Attributes exposing (src, value, type_, id, checked, name, rows, cols, class, for, style)
import Html.Events exposing (onClick, onInput, onSubmit, keyCode, on, onCheck)
import Markdown exposing (defaultOptions)
import Msgs
import Route
import Utils
import Types.Visibility as Visibility
import Types.Todo as Todo exposing (Todo)
import Types.ModalStatus as ModalStatus


todos : List Todo -> Visibility.Visibility -> List (Html Msgs.Msg)
todos todos visibility =
    filteredTodos todos visibility
        |> List.map todo


filteredTodos : List Todo -> Visibility.Visibility -> List Todo
filteredTodos todos visibility =
    case visibility of
        Visibility.All ->
            todos

        Visibility.Unfinished ->
            List.filter (\todo -> not todo.completed) todos

        Visibility.Completed ->
            List.filter (\todo -> todo.completed) todos


todo : Todo -> Html Msgs.Msg
todo todo =
    div [ class "row" ]
        (if todo.editing then
            [ div [ class "col-sm" ]
                [ editTodoForm todo
                ]
            ]
         else
            [ div [ class "col-sm-12 col-lg-8" ]
                [ completeCheckbox todo
                ]
            , div [ class "col-sm-12 col-lg-4" ]
                [ div [ class "button-group hidden-sm" ]
                    [ Route.routerLink [ class "button" ]
                        "View description"
                        (Route.TodoRoute todo.id)
                    , editTodoButton todo
                    , deleteTodoButton todo
                    ]
                , div [ class "button-group hidden-md hidden-lg" ]
                    [ button
                        [ type_ "button"
                        , onClick (Msgs.ChangeModalStatus (ModalStatus.Visible todo))
                        ]
                        [ text "Options" ]
                    ]
                ]
            ]
        )


singleTodo : Todo -> ModalStatus.ModalStatus -> Html Msgs.Msg
singleTodo todo modalStatus =
    div []
        [ case modalStatus of
            ModalStatus.Visible todo ->
                singleTodoActionModal todo

            ModalStatus.Hidden ->
                text ""

        -- Todo name and controls
        , if todo.editing then
            div [ class "row" ]
                [ div [ class "col-sm" ]
                    [ editTodoForm todo ]
                ]
          else
            div [ class "row" ]
                [ div [ class "col-sm" ]
                    [ h1 [] [ text todo.name ]
                    , hr [] []
                    ]
                ]
        , div [ class "row" ]
            [ div [ class "col-sm-6 col-md-2" ]
                [ label []
                    [ input
                        [ type_ "checkbox"
                        , checked todo.completed
                        , onCheck (flip Todo.UpdateCompletion todo.id >> Msgs.TodoMsg)
                        ]
                        []
                    , text " Completed "
                    ]
                ]
            , div [ class "col-sm-6 col-md-10" ]
                -- Options menu on larger screens
                [ div [ class "button-group hidden-sm" ]
                    [ editTodoButton todo
                    , deleteTodoButton todo
                    , button
                        [ onClick (Msgs.TodoMsg <| Todo.EditDescription todo.id)
                        ]
                        [ text "Edit description" ]
                    , Route.routerLink [ class "button" ] "Back to todos" Route.TodosRoute
                    ]

                -- Options button on mobile
                , div [ class "button-group hidden-md hidden-lg" ]
                    [ button
                        [ type_ "button"
                        , onClick (Msgs.ChangeModalStatus (ModalStatus.Visible todo))
                        ]
                        [ text "Options" ]
                    ]
                ]
            ]

        -- Description area
        , div [ class "row" ]
            [ div [ class "col-sm" ]
                (case todo.newDescription of
                    Nothing ->
                        [ Markdown.toHtmlWith markdownOptions
                            []
                            (Maybe.withDefault "" todo.description)
                        ]

                    Just newDescription ->
                        [ form [ onSubmit (Msgs.TodoMsg <| Todo.SaveNewDescription todo.id) ]
                            [ div [ class "input-group vertical" ]
                                [ label [ for "edit-description" ]
                                    [ text "New description (markdown is supported)" ]
                                , textarea
                                    [ onInput (flip Todo.NewDescription todo.id >> Msgs.TodoMsg )
                                    , value newDescription
                                    , style
                                        [ ( "width", "100%" )
                                        , ( "font-family", "monospace" )
                                        ]
                                    , rows
                                        (String.split "\n" newDescription
                                            |> List.length
                                        )
                                    , id "edit-description"
                                    ]
                                    []
                                ]
                            , button
                                [ type_ "submit"
                                , class "primary"
                                ]
                                [ text "Save" ]
                            , button
                                [ type_ "button"
                                , onClick (Msgs.TodoMsg <| Todo.CancelEditDescription todo.id)
                                ]
                                [ text "Cancel" ]
                            ]
                        ]
                )
            ]
        ]


completeCheckbox : Todo -> Html Msgs.Msg
completeCheckbox todo =
    label []
        [ input
            [ type_ "checkbox"
            , checked todo.completed
            , onCheck (flip Todo.UpdateCompletion todo.id >> Msgs.TodoMsg)
            ]
            []
        , text (" " ++ todo.name)
        ]


editTodoForm : Todo -> Html Msgs.Msg
editTodoForm todo =
    form [ onSubmit (Msgs.TodoMsg <| Todo.UpdateTodo todo.id) ]
        [ div [ class "input-group fluid" ]
            [ label [ for ("todo-" ++ toString todo.id) ] [ text "New name: " ]
            , input
                [ onInput (flip Todo.UpdateEdit todo.id >> Msgs.TodoMsg)
                , value todo.newName
                , Utils.onEscape (Msgs.TodoMsg <| Todo.CancelEdit todo.id)
                , id ("todo-" ++ toString todo.id)
                , type_ "text"
                ]
                []
            , button
                [ type_ "submit"
                , class "primary"
                ]
                [ text "Save" ]
            , cancelEditButton todo
            ]
        ]


deleteTodoButton : Todo -> Html Msgs.Msg
deleteTodoButton todo =
    button
        [ onClick (Msgs.TodoMsg <| Todo.DeleteTodo todo.id)
        , type_ "button"
        ]
        [ text "Delete" ]


editTodoButton : Todo -> Html Msgs.Msg
editTodoButton todo =
    button
        [ onClick (Msgs.TodoMsg <| Todo.EditTodo todo.id)
        , type_ "button"
        ]
        [ text "Edit name" ]


cancelEditButton : Todo -> Html Msgs.Msg
cancelEditButton todo =
    button
        [ onClick (Msgs.TodoMsg <| Todo.CancelEdit todo.id)
        , type_ "button"
        ]
        [ text "Cancel" ]


markdownOptions : Markdown.Options
markdownOptions =
    { defaultOptions | sanitize = True }


actionModal : Todo -> Html Msgs.Msg
actionModal todo =
    div
        [ class "modal-container"
        , onClick (Msgs.ChangeModalStatus ModalStatus.Hidden)
        ]
        [ div
            [ class "card"
            ]
            [ h3
                [ class "section" ]
                [ text "Todo menu" ]
            , div [ class "section" ]
                [ div [ class "button-group vertical" ]
                    [ Route.routerLink [ class "button" ]
                        "View description"
                        (Route.TodoRoute todo.id)
                    , editTodoButton todo
                    , deleteTodoButton todo
                    , button
                        [ type_ "button"
                        , onClick (Msgs.ChangeModalStatus ModalStatus.Hidden)
                        ]
                        [ text "Close" ]
                    ]
                ]
            ]
        ]


singleTodoActionModal : Todo -> Html Msgs.Msg
singleTodoActionModal todo =
    div
        [ class "modal-container"
        , onClick (Msgs.ChangeModalStatus ModalStatus.Hidden)
        ]
        [ div
            [ class "card"
            ]
            [ h3
                [ class "section" ]
                [ text "Todo menu" ]
            , div [ class "section" ]
                [ div [ class "button-group vertical" ]
                    [ editTodoButton todo
                    , button
                        [ type_ "button"
                        , onClick (Msgs.TodoMsg <| Todo.EditDescription todo.id)
                        ]
                        [ text "Edit description" ]
                    , deleteTodoButton todo
                    , Route.routerLink [ class "button" ]
                        "Back to todos"
                        (Route.TodosRoute)
                    , button
                        [ type_ "button"
                        , onClick (Msgs.ChangeModalStatus ModalStatus.Hidden)
                        ]
                        [ text "Close" ]
                    ]
                ]
            ]
        ]
