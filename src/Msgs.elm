module Msgs exposing (Msg(..))

import Navigation exposing (Location)
import Types.ModalStatus exposing (ModalStatus)
import Types.Todo exposing (Msg)
import Types.TodoFormVisibility as TodoFormVisibility
import Types.Visibility as Visibility
import Types.Visibility exposing (Visibility)


type Msg
    = NoOp
    | OnLocationChange Location
    | TodoMsg Types.Todo.Msg
    | AddNewTodo
    | ChangeLocation String
    | NewVisibility Visibility
    | UpdateNewTodo String
    | ChangeModalStatus ModalStatus
    | ChangeTodoFormVisibility TodoFormVisibility.TodoFormVisibility
    | TodosFromStorage (List Types.Todo.Todo)
