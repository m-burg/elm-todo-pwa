port module Main exposing (..)

import Html exposing (Html, Attribute, text, div, h1, img, p, button, form, input, label, fieldset, legend, h3, hr)
import Html.Attributes exposing (src, value, type_, id, checked, name, class, for)
import Html.Events exposing (onClick, onInput, onSubmit, keyCode, on, onCheck)
import Maybe
import Msgs
import Navigation exposing (Location)
import Page.Todos
import Route
import Types.ModalStatus as ModalStatus
import Types.Todo as Todo exposing (Todo, updateTodos)
import Types.TodoFormVisibility as TodoFormVisibility
import Types.Visibility as Visibility


---- MODEL ----


type alias Model =
    { todos : List Todo
    , newTodo : String
    , visibility : Visibility.Visibility
    , route : Route.Route
    , modalStatus : ModalStatus.ModalStatus
    , todoFormVisibility : TodoFormVisibility.TodoFormVisibility
    }


init : Maybe (List Todo) -> Location -> ( Model, Cmd Msgs.Msg )
init todoList location =
    let
        currentRoute =
            Route.parseLocation location
    in
        ( { initialModel
            | route = currentRoute
            , todos = Maybe.withDefault [] todoList
          }
        , if currentRoute == Route.HomeRoute then
            Navigation.modifyUrl (Route.pathFromRoute Route.TodosRoute)
          else
            Cmd.none
        )


initialModel : Model
initialModel =
    { todos = []
    , newTodo = ""
    , visibility = Visibility.All
    , route = Route.HomeRoute
    , modalStatus = ModalStatus.Hidden
    , todoFormVisibility = TodoFormVisibility.Hidden
    }


port saveTodos : List Todo -> Cmd msg



---- SUBSCRIPTIONS ----


port todosFromStorage : (List Todo -> msg) -> Sub msg

subscriptions : Model -> Sub Msgs.Msg
subscriptions model =
    todosFromStorage Msgs.TodosFromStorage

---- UPDATE ----


update : Msgs.Msg -> Model -> ( Model, Cmd Msgs.Msg )
update msg model =
    case msg of
        Msgs.NoOp ->
            ( model, Cmd.none )

        Msgs.ChangeLocation newLocation ->
            ( model, Navigation.newUrl newLocation )

        Msgs.OnLocationChange location ->
            let
                newRoute =
                    Route.parseLocation location
            in
                ( { model | route = newRoute }
                , if newRoute == Route.HomeRoute then
                    Navigation.modifyUrl (Route.pathFromRoute Route.TodosRoute)
                  else
                    Cmd.none
                )

        Msgs.NewVisibility visibility ->
            ( { model | visibility = visibility }, Cmd.none )

        Msgs.UpdateNewTodo newTodo ->
            ( { model | newTodo = newTodo }, Cmd.none )

        Msgs.ChangeModalStatus newStatus ->
            ( { model | modalStatus = newStatus }, Cmd.none )

        Msgs.ChangeTodoFormVisibility newVisibility ->
            ( { model | todoFormVisibility = newVisibility }, Cmd.none )

        Msgs.AddNewTodo ->
            let
                ( todos, cmd ) =
                    updateTodos model.todos (Todo.AddNewTodo model.newTodo)
            in
                ( { model
                    | todos = todos
                    , newTodo = ""
                  }
                , saveTodos todos
                )

        Msgs.TodoMsg msg ->
            let
                ( todos, cmd ) =
                    updateTodos model.todos msg
            in
                ( { model | todos = todos }
                , Cmd.batch ([ saveTodos todos ] ++ [ Cmd.map (\c -> Msgs.TodoMsg c) cmd ])
                )

        Msgs.TodosFromStorage todos ->
            ( { model | todos = todos }
            , Cmd.none
            )


markAsComplete : List Todo -> Int -> List Todo
markAsComplete tasks id =
    List.map
        (\task ->
            if task.id == id then
                { task | completed = not task.completed }
            else
                task
        )
        tasks



---- VIEW ----


view : Model -> Html Msgs.Msg
view model =
    div [ class "container" ]
        [ div [ class "col-sm-12 col-md-8 col-md-offset-2" ]
            (page model)
        ]


page : Model -> List (Html Msgs.Msg)
page model =
    case model.route of
        Route.HomeRoute ->
            [ div [] [ text "" ] ]

        Route.TodoRoute id ->
            [ todoView (getTodoWithId id model.todos) model.modalStatus ]

        Route.TodosRoute ->
            todosView
                model.todos
                model.visibility
                model.newTodo
                model.modalStatus
                model.todoFormVisibility

        Route.NotFoundRoute ->
            [ notFoundView ]


getTodoWithId : Int -> List Todo -> Maybe Todo
getTodoWithId id todos =
    List.filter (\todo -> todo.id == id) todos
        |> List.head


todosView :
    List Todo
    -> Visibility.Visibility
    -> String
    -> ModalStatus.ModalStatus
    -> TodoFormVisibility.TodoFormVisibility
    -> List (Html Msgs.Msg)
todosView todoList visibility newTodo modalStatus todoFormVisibility =
    [ div [ class "row" ] [ h1 [] [ text "Elm Todo PWA" ] ]

    -- Add todo form
    , div [ class "row hidden-sm" ]
        [ div [ class "col-sm" ]
            [ addTodoForm newTodo
            ]

        -- Change visibility
        , div [ class "col-sm" ]
            [ filterTodosForm visibility
            ]
        ]

    -- Mobile todo forms
    , div [ class "row hidden-md hidden-lg" ]
        [ div [ class "col-sm" ]
            (case todoFormVisibility of
                TodoFormVisibility.Hidden ->
                    [ div [ class "button-group" ]
                        [ button
                            [ type_ "button"
                            , onClick (Msgs.ChangeTodoFormVisibility TodoFormVisibility.AddNewTodo)
                            ]
                            [ text "Add new todo"
                            ]
                        , button
                            [ type_ "button"
                            , onClick (Msgs.ChangeTodoFormVisibility TodoFormVisibility.FilterTodos)
                            ]
                            [ text "Filter todos" ]
                        ]
                    ]

                TodoFormVisibility.AddNewTodo ->
                    [ button
                        [ type_ "button"
                        , onClick (Msgs.ChangeTodoFormVisibility TodoFormVisibility.Hidden)
                        ]
                        [ text "Cancel" ]
                    , addTodoForm newTodo
                    ]

                TodoFormVisibility.FilterTodos ->
                    [ button
                        [ type_ "button"
                        , onClick (Msgs.ChangeTodoFormVisibility TodoFormVisibility.Hidden)
                        ]
                        [ text "Cancel" ]
                    , filterTodosForm visibility
                    ]
            )
        ]

    -- Todo list
    , hr [] []
    , div [ class "row" ]
        [ div [ class "col-sm" ]
            (Page.Todos.todos todoList visibility)
        ]
    , case modalStatus of
        ModalStatus.Visible todo ->
            Page.Todos.actionModal todo

        ModalStatus.Hidden ->
            text ""
    ]


addTodoForm : String -> Html Msgs.Msg
addTodoForm newTodo =
    form [ onSubmit (Msgs.AddNewTodo) ]
        [ div [ class "input-group fluid" ]
            [ label
                [ for "new-todo"
                ]
                [ text "New todo: " ]
            , input
                [ onInput Msgs.UpdateNewTodo
                , value newTodo
                , id "new-todo"
                ]
                []
            , button
                [ type_ "submit"
                , class "primary"
                ]
                [ text "Add" ]
            ]
        ]


filterTodosForm : Visibility.Visibility -> Html Msgs.Msg
filterTodosForm visibility =
    form []
        [ fieldset []
            [ legend [] [ text "Filter tasks" ]
            , radio
                (Msgs.NewVisibility Visibility.All)
                "All"
                (visibility == Visibility.All)
            , radio
                (Msgs.NewVisibility Visibility.Completed)
                "Completed"
                (visibility == Visibility.Completed)
            , radio
                (Msgs.NewVisibility Visibility.Unfinished)
                "Unfinished"
                (visibility == Visibility.Unfinished)
            , button
                [ type_ "button"
                , onClick (Msgs.TodoMsg <| Todo.DeleteCompletedTodos)
                ]
                [ text "Delete completed todos" ]
            ]
        ]


todoView : Maybe Todo -> ModalStatus.ModalStatus -> Html Msgs.Msg
todoView todo modalStatus =
    case todo of
        Just todo ->
            Page.Todos.singleTodo todo modalStatus

        Nothing ->
            div []
                [ p []
                    [ text "Todo not found. "
                    , Route.routerLink [] "Back to todos" Route.TodosRoute
                    ]
                ]


notFoundView : Html Msgs.Msg
notFoundView =
    div []
        [ p []
            [ text "Page not found. "
            , Route.routerLink [] "Home" Route.HomeRoute
            ]
        ]


radio : Msgs.Msg -> String -> Bool -> Html Msgs.Msg
radio msg value isChecked =
    label []
        [ input
            [ type_ "radio"
            , onClick msg
            , checked isChecked
            , name "visibility"
            ]
            []
        , text value
        ]



---- PROGRAM ----


main : Program (Maybe (List Todo)) Model Msgs.Msg
main =
    Navigation.programWithFlags Msgs.OnLocationChange
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
