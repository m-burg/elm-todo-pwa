module Types.TodoFormVisibility exposing (TodoFormVisibility(..))

type TodoFormVisibility
    = Hidden
    | AddNewTodo
    | FilterTodos
