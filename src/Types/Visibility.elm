module Types.Visibility exposing (Visibility(..))


type Visibility
    = All
    | Unfinished
    | Completed
