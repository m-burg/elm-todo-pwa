module Types.ModalStatus exposing (ModalStatus(..))

import Types.Todo exposing (Todo)


type ModalStatus
    = Hidden
    | Visible Todo
