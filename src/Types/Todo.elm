module Types.Todo exposing (Todo, Msg(..), updateTodos)

import Dom
import Task
import String exposing (trim, isEmpty)


type alias Todo =
    { name : String
    , completed : Bool
    , id : Int
    , editing : Bool
    , newName : String
    , description : Maybe String
    , newDescription : Maybe String
    }


updateTodos : List Todo -> Msg -> ( List Todo, Cmd Msg )
updateTodos todos msg =
    case msg of
        NoOp ->
            ( todos, Cmd.none )

        UpdateCompletion newStatus id ->
            let
                updateStatus t =
                    if t.id == id then
                        { t | completed = newStatus }
                    else
                        t
            in
                ( List.map updateStatus todos
                , Cmd.none
                )

        AddNewTodo name ->
            if not <| isEmpty <| trim name then
                ( newTodo name todos :: todos
                , Cmd.none
                )
            else
                ( todos
                , Cmd.none
                )

        DeleteTodo id ->
            ( List.filter (\todo -> todo.id /= id) todos
            , Cmd.none
            )

        EditTodo id ->
            let
                editTodo t =
                    if t.id == id then
                        { t
                            | editing = True
                            , newName = t.name
                        }
                    else
                        t

                focus =
                    Dom.focus ("todo-" ++ toString id)
            in
                ( List.map editTodo todos
                , Task.attempt (\_ -> NoOp) focus
                )

        UpdateEdit newEdit id ->
            let
                updateEdit t =
                    if t.id == id then
                        { t | newName = newEdit }
                    else
                        t
            in
                ( List.map updateEdit todos
                , Cmd.none
                )

        UpdateTodo id ->
            let
                saveTodo t =
                    if t.id == id then
                        { t
                            | name = t.newName
                            , newName = ""
                            , editing = False
                        }
                    else
                        t
            in
                ( List.map saveTodo todos
                , Cmd.none
                )

        CancelEdit id ->
            let
                cancelEdit t =
                    if t.id == id then
                        { t
                            | newName = ""
                            , editing = False
                        }
                    else
                        t
            in
                ( List.map cancelEdit todos
                , Cmd.none
                )

        EditDescription id ->
            let
                editDescription t =
                    if t.id == id then
                        { t
                            | newDescription =
                                Just (Maybe.withDefault "" t.description)
                        }
                    else
                        t
            in
                ( List.map editDescription todos
                , Task.attempt (\_ -> NoOp) (Dom.focus "edit-description")
                )

        CancelEditDescription id ->
            let
                cancelEditDescription t =
                    if t.id == id then
                        { t | newDescription = Nothing }
                    else
                        t
            in
                ( List.map cancelEditDescription todos
                , Cmd.none
                )

        NewDescription description id ->
            let
                newDescription t =
                    if t.id == id then
                        { t | newDescription = Just description }
                    else
                        t
            in
                ( List.map newDescription todos
                , Cmd.none
                )

        SaveNewDescription id ->
            let
                saveDescription t =
                    if t.id == id then
                        { t
                            | description = t.newDescription
                            , newDescription = Nothing
                        }
                    else
                        t
            in
                ( List.map saveDescription todos
                , Cmd.none
                )

        DeleteCompletedTodos ->
            ( List.filter (\todo -> not todo.completed) todos
            , Cmd.none
            )


newTodo : String -> List Todo -> Todo
newTodo taskName existingTodos =
    Todo taskName False (getHighestTodoId existingTodos + 1) False "" Nothing Nothing


getHighestTodoId : List Todo -> Int
getHighestTodoId tasks =
    tasks
        |> List.map .id
        |> List.maximum
        |> Maybe.withDefault 0


type Msg
    = NoOp
    | UpdateCompletion Bool Int
    | AddNewTodo String
    | DeleteTodo Int
    | EditTodo Int
    | UpdateEdit String Int
    | UpdateTodo Int
    | CancelEdit Int
    | EditDescription Int
    | CancelEditDescription Int
    | NewDescription String Int
    | SaveNewDescription Int
    | DeleteCompletedTodos
