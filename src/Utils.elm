module Utils exposing (onEscape)

import Html exposing (Attribute)
import Html.Events exposing (on, keyCode)
import Json.Decode as Json
import Msgs


onEscape : Msgs.Msg -> Attribute Msgs.Msg
onEscape message =
    let
        isEscape code =
            if code == 27 then
                Json.succeed message
            else
                Json.fail "Not Escape"
    in
        on "keydown" (Json.andThen isEscape keyCode)
